<?php
    /*
    Plugin Name: First Plugin
    Plugin URI: http://test_domain.com
    Description: First plugin.
    Version: 1.0
    Author: MihailCh
    Author URI: http://test_domain.com
    */

    /*  Copyright 2022  MihailCh  (email: user@mail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/** after comment add div */

function function_for_title( $title ){
    $title = '<span class="clicable-linck">NEW</span> ' . $title;
    return $title;
}

function load_clicable_linck(){
    wp_register_style( 'span_style', plugins_url( 'span_new_color.css',__FILE__  ) );
    wp_enqueue_style( 'span_style' );
    wp_register_script( 'active_script', plugins_url( 'change_color_on_linck.js',__FILE__ ), array( 'jquery' ) );
    wp_enqueue_script( 'active_script' );
}

function add_div_2_comment( $comment ){
    $comment = $comment . '<div>Some string.</div>';
    return $comment;
}

add_filter( 'the_title', 'function_for_title' );
add_filter( 'comment_text', 'add_div_2_comment' );

add_action( 'init', 'load_clicable_linck' );
?>